[![Typing SVG](https://readme-typing-svg.herokuapp.com?font=Fira+Code&size=14&pause=1000&center=true&width=500&height=100&lines=%F0%9F%9A%80+Tech+Architect+%7C+Full+Stack+Creator+;UX+Evangelist+%F0%9F%8E%A8;Experiences+with+C%2B%2B%2C+PHP%2C+Node.js%2C+Python%2C+Go+%26+DevOps;Turning+Visions+into+Reality+%40+OnestTech+%F0%9F%8C%9F)](https://git.io/typing-svg)


<h2>  Hi there! 👋 I'm Md Rasheduzzaman. </h2>

Hello and welcome to my profile! As an experienced full-stack developer with over 6 years of experience, I specialize in programming languages such as C++, Python, Go, and C, PHP. I am proficient in MERN and LAMP stacks and have expertise in using DevOps tools like AWS, Docker, Jenkins, and Git.

I am a problem solver at heart, and I enjoy taking on challenging projects that push me to think outside the box. Throughout my career, I have successfully delivered top-notch software solutions that meet the needs of my clients.

In addition to my technical skills, I also possess excellent communication and collaboration skills. I believe that effective communication is key to ensuring that a project is successful, and I always strive to keep my clients in the loop every step of the way.

If you're looking for a dedicated and skilled developer for your next project, please don't hesitate to reach out to me. I'd be more than happy to help bring your vision to life.

Here are some ideas to knowing me:

- 🔭 I’m currently working on PHP, Laravel, Javascript, Python, Go
- 💬 Ask me about Go, PHP, Python, C/C++, Laravel and API etc.
- 📫 How to reach me: jmrashed@gmail.com


 I have completed several recent projects that demonstrate my ability to deliver successful software solutions.
- HRM (Human Resource Management) 
- CRM (Customer Relationship Management)
- LMS (Learning Management System)
- Music Streaming
- Chatbot using open AI
- Ecommerce applications
- Face Recognition Attendance System
- Video Calling Web Application

<h1><a href="Resume.md">Resume</a></h1>


[![Check out jmrashed's profile on stardev.io](https://stardev.io/developers/jmrashed/badge/languages/global.svg)](https://stardev.io/developers/jmrashed)


### The most commonly used programming languages

![C](https://img.shields.io/badge/C-F7DF1E?style=flat-square&logo=C&logoColor=white)
![C++](https://img.shields.io/badge/C++-004482?style=flat-square&logo=C++&logoColor=white)
![Python](https://img.shields.io/badge/Python-306998?style=flat-square&logo=python&logoColor=white)
![Go](https://img.shields.io/badge/Go-29BEB0?style=flat-square&logo=go&logoColor=white)
![PHP](https://img.shields.io/badge/PHP-777BB4?style=flat-square&logo=php&logoColor=white)

 
## Used Technologies
![Linux](https://img.shields.io/badge/Linux-00C7B7?style=flat-square&logo=linux&logoColor=white)
![MySQL](https://img.shields.io/badge/MySQL-005C84?style=flat-square&logo=mysql&logoColor=white)
![Apache](https://img.shields.io/badge/Apache-C51A38?style=flat-square&logo=apache&logoColor=white)
![MariaDB](https://img.shields.io/badge/MariaDB-005C84?style=flat-square&logo=mariadb&logoColor=white)
![Python](https://img.shields.io/badge/Python-3776AB?style=flat-square&logo=python&logoColor=white)
![Nginx](https://img.shields.io/badge/Nginx-005C84?style=flat-square&logo=Nginx&logoColor=blue)
![Docker](https://img.shields.io/badge/Docker-0CC1F3?style=flat-square&logo=docker&logoColor=white)
![Slack](https://img.shields.io/badge/slack-2EB67D?style=flat-square&logo=slack&logoColor=white)
![Git](https://img.shields.io/badge/git-F1502F?style=flat-square&logo=git&logoColor=white)
![GitLab](https://img.shields.io/badge/gitLab-8C929D?style=flat-square&logo=gitLab&logoColor=white)
![GitHub](https://img.shields.io/badge/github-171515?style=flat-square&logo=github&logoColor=white)
![Bitbucket](https://img.shields.io/badge/bitbucket-253858?style=flat-square&logo=bitbucket&logoColor=white)
![AWS](https://img.shields.io/badge/aws-F79400?style=flat-square&logo=aws&logoColor=white)
![Graphql](https://img.shields.io/badge/Graphql-007ACC?style=flat-square&logo=graphql&logoColor=red)
![Redis](https://img.shields.io/badge/redis-%23DD0031.svg?&style=flat-square&logo=redis&logoColor=white)
![Cloudflare](https://img.shields.io/badge/Cloudflare-F38020?style=flat-square&logo=Cloudflare&logoColor=white)
![SQLite](https://img.shields.io/badge/SQLite-07405E?style=flat-square&logo=sqlite&logoColor=white)
![HTML](https://img.shields.io/badge/HTML5-E34F26?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/CSS3-1572B6?style=flat-square&logo=css3&logoColor=white)
![JavaScript](https://img.shields.io/badge/JavaScript-F7DF1E?style=flat-square&logo=javascript&logoColor=black)
![jQuery](https://img.shields.io/badge/jQuery-0769AD?style=flat-square&logo=jquery&logoColor=white)
![Bootstrap](https://img.shields.io/badge/Bootstrap-563D7C?style=flat-square&logo=bootstrap&logoColor=white)
![Tailwind](https://img.shields.io/badge/tailwindcss-0081CB?style=flat-square&logo=tailwindcss&logoColor=white)
![Ionic](https://img.shields.io/badge/ionic-563D7C?style=flat-square&logo=ionic&logoColor=white)
![Laravel](https://img.shields.io/badge/Laravel-FF2D20?style=flat-square&logo=laravel&logoColor=white)

 




## Where to find me

[![Linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=flat-square&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/md-rasheduzzaman/)
[![Twitter](https://img.shields.io/badge/Twitter-1DA1F2?style=flat-square&logo=twitter&logoColor=white)](https://twitter.com/_rasheduzzaman)
[![Facebook](https://img.shields.io/badge/Facebook-1877F2?style=flat-square&logo=facebook&logoColor=white)](https://www.facebook.com/engr.jmrashed.bd)
[![Reddit](https://img.shields.io/badge/reddit-FF4500?style=flat-square&logo=reddit&logoColor=white)](https://www.reddit.com/user/md-rasheduzzaman)
[![Stackoverflow](https://img.shields.io/badge/stackoverflow-F48024?style=flat-square&logo=stackoverflow&logoColor=white)](https://stackoverflow.com/users/3699981/rashed-zaman)
[![codepen](https://img.shields.io/badge/codepen-F48024?style=flat-square&logo=codepen&logoColor=white)](https://codepen.io/jmrashed)
[![devTo](https://img.shields.io/badge/dev.to-000000?style=flat-square&logo=dev&logoColor=white)](https://dev.to/jmrashed)
[![codesandbox](https://img.shields.io/badge/codesandbox-000000?style=flat-square&logo=codesandbox&logoColor=white)](https://codesandbox.com/jmrashed)
[![instagram](https://img.shields.io/badge/instagram-red?style=flat-square&logo=instagram&logoColor=white)](https://instagram.com/jmrashed)
[![dribbble](https://img.shields.io/badge/dribbble-ea4c89?style=flat-square&logo=dribbble&logoColor=white)](https://codesandbox.com/jmrashed)
[![behance](https://img.shields.io/badge/behance-053eff?style=flat-square&logo=behance&logoColor=white)](https://www.behance.net/jmrashed)



## Experience
### Tech Lead Officer | Onest Tech LLC
📍 Dhaka, Bangladesh | 📅 September 2022 - Present

- **Strategic Innovation:** Orchestrated a dynamic team to conceptualize, execute, and refine avant-garde software solutions.
- **Performance Enhancement:** Spearheaded mission-critical app development, curbing latency by 40% through precision algorithmic optimizations.
- **Mentorship Champion:** Nurtured junior programmers, accelerating their growth curve and magnifying overall team productivity.
- **Design Synergy:** Collaborated with UX designers, sculpting user-centric interfaces that elevate the realm of user experience.

### Senior Software Project Manager | Onest Tech LLC
📍 Dhaka, Bangladesh | 📅 January 2021 - August 2022

- **Holistic Leadership:** Guided a cross-functional team in orchestrating end-to-end realization of innovative software solutions.
- **Latency Cruncher:** Pioneered mission-critical app refinement, slashing latency by 40% through meticulous algorithmic enhancements.
- **Mentor Extraordinaire:** Cultivated emerging programmers, catalyzing their professional growth and team prowess.
- **UX Elevation:** Partnered with UX designers to craft interfaces that redefine user experience.

### Software Project Manager | Spondon IT Ltd.
📍 Dhaka, Bangladesh | 📅 April 2019 - December 2020

- **Innovative Engineering:** Revolutionized backend services with Python and Node.js, elevating system reliability by 25%.
- **Seamless Integration:** Engineered RESTful APIs for frictionless service communication, amplifying system modularity and agility.
- **Architectural Revamp:** Led the migration of legacy code to modern architecture, augmenting code maintainability.

## Education
### B.Sc. in Computer Science | Stamford University Bangladesh 
📍 Dhaka, Bangladesh | 📅 Graduated May 2016

- **Key Studies:** Algorithms and Data Structures, Object-Oriented Programming, Software Engineering.
- **Capstone Triumph:** Conceptualized an optimized real-time chat app using C++ and WebSocket technology.

## Skills
- **Languages:** C++, Python, PHP, Node.js, Go.
- **Algorithmic Expertise:** Crafting optimized algorithms for intricate problems.
- **Code Maestro:** Proficient at spotting bottlenecks, enhancing code efficiency.
- **Full Stack Fusion:** Masterful in crafting end-to-end web applications.
- **DevOps Dynamo:** Proficient with Docker, Kubernetes, and orchestrating deployments.

## Projects
### Real-Time Chat Application
🌐 [GitHub Repository](https://github.com/johndoe/chat-app)

- Engineered real-time chat app in C++ and WebSocket tech.
- Multi-threaded message delivery and latency slashing with priority queues.

## Certifications
- AWS Certified Developer - Associate
- Certified ScrumMaster

## Awards
- Innovator of the Year, Onest Tech LLC (2022)
- Code Optimization Champion, CodingCon 2020

## Contact
📧 Email: jmrashed@gmail.com
📍 Location: Dhaka, Bangladesh
